import Vue from "vue";
import VueRouter from "vue-router";

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("../views/xinqing/X-index.vue"),
    meta: { show: true },
  },
  // 登录注册页面
  {
    path: "/lo-re",
    name: "lo-re",
    component: () => import("../views/lo-re.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/login.vue"),
  },
  {
    path: "/register",
    name: "register",
    component: () => import("../views/register.vue"),
  },
  {
    path: "/jieyou",
    name: "jieyou",
    component: () => import("../views/jieyou/J-index.vue"),
    meta: { show: true },
  },
  {
    path: "/xinqing",
    name: "xinqing",
    component: () => import("../views/xinqing/X-index.vue"),
    meta: { show: true },
  },
  {
    path: "/hudong",
    name: "hudong",
    component: () => import("../views/hudong/H-index.vue"),
    redirect: "/hudong/qun",
    children: [
      {
        path: "qun",
        name: "qun",
        component: () => import("../views/hudong/H-qun.vue"),
        meta: { show: true },
      },
      {
        path: "si",
        name: "si",
        component: () => import("../views/hudong/H-si.vue"),
        meta: { show: true },
      },
    ],
  },
  {
    path: "/wode",
    name: "wode",
    component: () => import("../views/wode/W-index.vue"),
    meta: { show: true },
  },

  // 用户详情页
  {
    path: "/zyperson",
    name: "zyperson",
    component: () => import("../views/zyperson.vue"),
  },

  // 树洞模块
  {
    path: "/shudong",
    name: "shudong",
    redirect: "/shudong/ground",
    component: () => import("../views/shudong/S-index.vue"),
    children: [
      {
        path: "ground",
        name: "ground",
        component: () => import("../views/shudong/S-Ground.vue"),
        meta: { show: true },
      },
      {
        path: "focus",
        name: "focus",
        component: () => import("../views/shudong/S-Focus.vue"),
        meta: { show: true },
      },
      {
        path: "us",
        name: "us",
        component: () => import("../views/shudong/S-Us.vue"),
        meta: { show: true },
      },
    ],
  },
  {
    path: "/release",
    name: "release",
    component: () => import("../views/shudong/S-Release.vue"),
  },
  {
    path: "/search",
    name: "search",
    component: () => import("../views/shudong/S-Search.vue"),
    // meta:{ keepAlive: true}
  },
  {
    path: "/details",
    name: "details",
    redirect: "/details/comments",
    component: () => import("../views/shudong/S-Details.vue"),
    children: [
      {
        path: "comments",
        name: "comments",

        component: () => import("../views/shudong/S-Comments.vue"),
      },
      {
        path: "hug",
        name: "hug",
        component: () => import("../views/shudong/S-Hug.vue"),
      },
    ],
  },


  
  // 心情模块
  {
    path: "/xinqing/lianxi",
    name: "lianxi",
    component: () => import("../views/xinqing/X-lianxi.vue"),
  },
  {
    path: "/xinqing/audio",
    name: "audio",
    component: () => import("../views/xinqing/X-audio.vue"),
  },
  {
    path: "/xinqing/share",
    name: "share",
    component: () => import("../views/xinqing/X-share.vue"),
  },
  {
    path: "/xinqing/option",
    name: "option",
    component: () => import("../views/xinqing/X-option.vue"),
  },
  {
    path: "/xinqing/daka",
    name: "daka",
    component: () => import("../views/xinqing/X-daka.vue"),
  },
  {
    path: "/xinqing/collect",
    name: "collect",
    component: () => import("../views/xinqing/X-collect.vue"),
  },

  // 设置模块
  {
    path: "/shezhi",
    name: "shezhi",
    component: () => import("../views/W-shezhi/shezhi-index.vue"),
  },
  {
    path: "/shezhi/xiugai",
    name: "xiugai",
    component: () => import("../views/W-shezhi/sz-xiugai.vue"),
  },
  {
    path: "/shezhi/sz-xiugai-name",
    name: "sz-xiugai-name",
    component: () => import("../views/W-shezhi/sz-xiugai-name.vue"),
  },
  {
    path: "/shezhi/sz-xiugai-qm",
    name: "sz-xiugai-qm",
    component: () => import("../views/W-shezhi/sz-xiugai-qm.vue"),
  },
  {
    path: "/shezhi/sz-xiugai-t",
    name: "sz-xiugai-t",
    component: () => import("../views/W-shezhi/sz-xiugai-t.vue"),
  },
  {
    path: "/shezhi/sz-mima",
    name: "mima",
    component: () => import("../views/W-shezhi/sz-mima.vue"),
  },
  {
    path: "/shezhi/sz-mima/wangji",
    name: "wangji",
    component: () => import("../views/W-shezhi/sz-mima-wangji.vue"),
  },
  {
    path: "/shezhi/sz-xiaoxi",
    name: "xiaoxi",
    component: () => import("../views/W-shezhi/sz-xiaoxi.vue"),
  },
  {
    path: "/shezhi/sz-yinsi",
    name: "yinsi",
    component: () => import("../views/W-shezhi/sz-yinsi.vue"),
  },
  {
    path: "/shezhi/sz-hei",
    name: "hei",
    component: () => import("../views/W-shezhi/sz-hei.vue"),
  },
  {
    path: "/shezhi/sz-tong",
    name: "tong",
    component: () => import("../views/W-shezhi/sz-tong.vue"),
  },
  {
    path: "/shezhi/sz-bang",
    name: "bang",
    component: () => import("../views/W-shezhi/sz-bang.vue"),
  },
  {
    path: "/shezhi/sz-guan",
    name: "guan",
    component: () => import("../views/W-shezhi/sz-guan.vue"),
  },
  {
    path: "/shezhi/sz-ban",
    name: "ban",
    component: () => import("../views/W-shezhi/sz-ban.vue"),
  },
  {
    path: "/shezhi/sz-guan-xieyi",
    name: "guan-xieyi",
    component: () => import("../views/W-shezhi/sz-guan-xieyi.vue"),
  },
  {
    path: "/shezhi/sz-guan-yinsi",
    name: "guan-yinsi",
    component: () => import("../views/W-shezhi/sz-guan-yinsi.vue"),
  },


  
  // 我的模块
  {
    path: "/wode/attention",
    name: "attention",
    component: () => import("../views/wode/W-attention.vue"),
  },
  {
    path: "/wode/attentioned",
    name: "attentioned",
    component: () => import("../views/wode/W-attentioned.vue"),
  },
  {
    path: '/wode/run',
    name: 'run',
    component: () => import('../views/wode/W-run.vue'),
    children:[
      {
        path:'detail',
        name:'detail',
        component:()=>import('../views/wode/W-detail.vue'),
      },
    ]
  },
  {
    path: '/wode/letter',
    name: 'letter',
    component: () => import('../views/wode/W-letter.vue'),
    redirect: "/wode/letter/ask",
    children: [
      {
        path: "ask",
        name: "ask",
        component: () => import("../views/wode/W-Ask.vue"),
      },,
      {
        path: "answer",
        name: "answer",
        component: () => import("../views/wode/W-Answer.vue"),
      },
    ],
  },
  {
    path: '/letter/ask/write',
    name: 'write',
    component: () => import('../views/wode/W-write.vue'),
  },
  {
    path: '/wode/detail',
    name: 'detail',
    component: () => import('../views/wode/W-detail.vue'),
  },
  
  {
    path: "/wode/radio",
    name: "radio",
    component: () => import("../views/wode/W-radio.vue"),
  },
  {
    path: "/wode/daily",
    name: "daily",
    component: () => import("../views/wode/W-daily.vue"),
  },
  {
    path: '/wode/daily/dailydetail',
    name: 'dailydetail',
    component: () => import('../views/wode/W-dailydetail.vue'),
  },
  {
    path: '/wode/daily/diaryw',
    name: 'diaryw',
    component: () => import('../views/wode/W-diaryw.vue'),
  },
  {
    path: "/wode/FM",
    name: "FM",
    component: () => import("../views/wode/W-FM.vue"),
  },
  {
    path: "/wode/FM/audio",
    name: "audio",
    component: () => import("../views/wode/W-audio.vue"),
  },
  {
    path: "/wode/FM/add",
    name: "add",
    component: () => import("../views/wode/W-add.vue"),
  },
  {
    path: "/wode/FM/add/music",
    name: "music",
    component: () => import("../views/wode/W-music.vue"),
  },
  {
    path: "/wode/past",
    name: "past",
    component: () => import("../views/wode/W-past.vue"),
  },
  {
    path: "/wode/past/pastwrite",
    name: "pastwrite",
    component: () => import("../views/wode/W-pastwrite.vue"),
  },
  {
    path: "/wode/past/pastwrite/:id",
    name: "pastwrite",
    component: () => import("../views/wode/W-pastwrite.vue"),
  },
  {
    path: "/wode/future",
    name: "future",
    component: () => import("../views/wode/W-future.vue"),
  },
  {
    path: "/wode/future/futurewrite",
    name: "futurewrite",
    component: () => import("../views/wode/W-futurewrite.vue"),
  },
  {
    path: "/wode/future/futurewrite/:id",
    name: "futurewrite",
    component: () => import("../views/wode/W-futurewrite.vue"),
  },
  {
    path: "/wode/circle",
    name: "circle",
    component: () => import("../views/wode/W-circle.vue"),
  },
  {
    path: "/wode/seek",
    name: "seek",
    component: () => import("../views/wode/W-seek.vue"),
  },
  {
    path: "/wode/about",
    name: "about",
    component: () => import("../views/wode/W-about.vue"),
  },


  // 解忧模块
  {
    path: "/rongyu",
    name: "rongyu",
    component: () => import("../views/jieyou/J-Rongyu.vue"),
  },
  {
    path: "/xinjian/:id",
    name: "xinjian",
    component: () => import("../views/jieyou/J-Xinjian.vue"),
  },
  {
    path: "/yingye",
    name: "yingye",
    component: () => import("../views/jieyou/J-Yingye.vue"),
  },
  {
    path: "/sorrowl",
    name: "xiangqing",
    component: () => import("../views/jieyou/J-GieSorrowl.vue"),
  },
  {
    path: "/zixun",
    name: "zixun",
    component: () => import("../views/jieyou/J-Zixun.vue"),
  },
  {
    path: "/xiangqing-ask",
    name: "xiangqing",
    component: () => import("../views/jieyou/J-xiangqing-ask.vue"),
  },
  {
    path: "/xiangqing-answer",
    name: "xiangqing",
    component: () => import("../views/jieyou/J-xiangqing-answer.vue"),
  },
  {
    path: "/letter",
    name: "letter",
    redirect: "/letter/ask",
    component: () => import("../views/jieyou/J-Letter.vue"),
    redirect: "/letter/ask",
    children: [
      {
        path: "ask",
        name: "ask",
        component: () => import("../views/jieyou/J-Ask.vue"),
      },
      {
        path: "answer",
        name: "answer",
        component: () => import("../views/jieyou/J-Answer.vue"),
      },
    ],
  },


  // 互动
  {
    path:'/chat',
    name: 'chat',
    component: ()=> import('../views/hudong/H-chat.vue')
  },
  {
    path:'/detail-s',
    name: 'detail-s',
    component: ()=> import('../views/hudong/H-detail-s.vue')
  },
  {
    path:'/detail-x',
    name: 'detail-x',
    component: ()=> import('../views/hudong/H-detail-x.vue')
  },





  {
    path:'/inagg',
    name: 'inagg',
    component: ()=> import('../views/hudong/inagg.vue')
  }
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// 全局的前置导航守卫，当路由跳转前，执行这个该函数
import store from '../store'
let islogin = store.state.user
const whitelist = [
  '/',
  '/jieyou',
  '/xinqing',
  '/shudong/ground',
  '/hudong/qun',
  '/wode',
  '/login',
  '/register',
  '/shezhi/sz-mima/wangji'
]
router.beforeEach((to,from,next)=>{
  // console.log('from',from);
  // console.log('to',to);
  if(whitelist.includes(to.path)){
    next()
  }else{
    // 如果跳到同意协议，如果里面全局，就继续
    if (to.path == "/lo-re") {
      // 如果没登录，就继续
      if (islogin == null) next()
      //  else next('/')
      } else {
    // 没跳到同意协议，如果没登录，就去同意协议
      if (islogin == null) next("/lo-re")
      // 如果登登录了，就继续
       else next()
     }

     
     // next()
    }
    // if(from.path == "/login" && to.path == "/wode" ){
    //   // next()
    //   this.$router.go(0)
    // }
})

export default router;
