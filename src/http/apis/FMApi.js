import myaxios from "../MyAxios";
import BASEURL from '../BaseUrl'
const {BMDURL} = BASEURL

const fmApi={
    // 查询所有音乐
  queryAll() {
    let url = BMDURL + '/FM'
    return myaxios.get(url)
  },


  // queryAllNew() {
  //   let url = BMDURL + '/FMNew'
  //   return myaxios.get(url)
  // },


  /**
   * 新增FM
   * @param {Object} params 
   */
   add(params) {
    return myaxios.post(BMDURL + '/FM/add', params)
  },

    /**
   *  通过类型查询音乐
   * @param {Object} params 
   * @returns
   */
     queryByTypeLike(params) {
      
      return myaxios.get(BMDURL + '/FM/query', params)
  },
 /**
   *  通过id查询音乐
   * @param {Object} params 
   * @returns
   */
  queryByIDLike(params) {    
    return myaxios.get(BMDURL + '/audio_id', params)
},
  queryAllRecommend() {
    let url = BMDURL + '/FMRecommend'
    return myaxios.get(url)
  },

  queryAllNew() {
    let url = BMDURL + '/FMNew'
    return myaxios.get(url)
  },
    /**
   * 新增音乐
   * @param {Object} params 
   */
  add(params) {
    return myaxios.post(BMDURL + '/FM/add', params)
  },
   

}
export default fmApi