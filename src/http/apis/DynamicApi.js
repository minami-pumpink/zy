/** 该文件仅存放动态模块相关的接口API */
import myaxios from "../MyAxios";
// 引入路径前缀
import BASEURL from "../BaseUrl";
const { BMDURL } = BASEURL;

const dynamicApi = {

  /** 查询所有动态 */
  queryAll() {
    return myaxios.get(BMDURL + "/dynamic");
  },

  /** 查询所有动态的cid */
  querycid() {
    return myaxios.get(BMDURL + "/cid");
  },

  /** 按名字模糊查询动态 */
  queryByname(params) {
    return myaxios.post(BMDURL + "/dynamic/nname", params);
  },

  /** 按cid查询动态 */
  queryByid(params) {
    return myaxios.get(BMDURL + "/details", params);
  },

  /** 按uid查询动态 */
  queryByuid(params) {
    return myaxios.get(BMDURL + "/u", params);
  },

  /** 新增动态 */
  add(params) {
    return myaxios.post(BMDURL + "/dynamic/add", params);
  },

  /** 新增评论 */
  pinglunadd(params) {
    return myaxios.post(BMDURL + "/pinglun/add", params);
  },

  /** 按cid查询评论 */
  querypinglun(params) {
    return myaxios.get(BMDURL + "/pinglun", params);
  },

};

export default dynamicApi;
