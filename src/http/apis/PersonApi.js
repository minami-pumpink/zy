/** 该文件仅存放演员模块相关的接口API */
import myaxios from "../MyAxios";
// 引入路径前缀
import BASEURL from '../BaseUrl' 
const {BMDURL} = BASEURL

const actorApi = {

  // /** 查询所有用户 */
  // queryAll() {
  //   let params = { page: 1, pagesize: 10 };
  //   return myaxios.get(BMDURL+'/zy', params);
  // },

  /** 查询 关注人数 */
  querygzid(params) {
    return myaxios.get(BMDURL+'/gzid', params);
  },

  /** 查询 关注的详情 */
  querygz(params) {
    return myaxios.get(BMDURL+'/gz', params);
  },

  /** 查询 被 关注人数 */
  querybgzid(params) {
    return myaxios.get(BMDURL+'/bgzid', params);
  },

  /** 查询 被 关注的详情 */
  querybgz(params) {
    return myaxios.get(BMDURL+'/bgz', params);
  },

}

export default actorApi;