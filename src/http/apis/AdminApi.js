// 登录注册修改密码
import myaxios from "../MyAxios";
// 引入路径前缀
import BASEURL from '../BaseUrl' 
const {BMDURL} = BASEURL

const adminApi = {

  /** 登录接口 */
  login(params) {
    return myaxios.post(BMDURL + "/login",params)
  },

  /** 注册接口 */
  register(params) {
    return myaxios.post(BMDURL + "/register",params)
  },

  /** 更新名字接口 */
  upname(params) {
    return myaxios.post(BMDURL + "/updataName",params)
  },

  /** 更新昵称接口 */
  ups(params) {
    return myaxios.post(BMDURL + "/updataS",params)
  },

  /** 更新当前用户信息接口 */
  getUser(params) {
    return myaxios.post(BMDURL + "/user",params)
  },

}

export default adminApi;