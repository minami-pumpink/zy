/** 该文件仅存放演员模块相关的接口API */
import myaxios from "../MyAxios";
// 引入路径前缀
import BASEURL from '../BaseUrl' 
const {BMDURL} = BASEURL

const moodApi = {

  /** 查询所有表情 */
  queryMoodAll() {
    return myaxios.get(BMDURL + '/moods')
  },

  /**
   * 通过id查询心情
   * @param: 
   * params: 存放影院id的对象 {id: 2}
   */
  queryMoodById(params){
    return myaxios.get(BMDURL + '/moods_query', params)
  },

  /**
   * 通过id查询音频
   * @param: 
   * params: 存放音频id的对象 {id: 2}
   */
   queryAudioById(params){
    return myaxios.get(BMDURL + '/audio_id', params)
  },

  // 查询所有原因
  queryReasonAll(){
    return myaxios.get(BMDURL + '/reasons')
  }

}

export default moodApi;