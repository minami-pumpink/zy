/** 该文件仅存放演员模块相关的接口API */
import myaxios from "../MyAxios";
// 引入路径前缀
import BASEURL from '../BaseUrl' 
const {BMDURL} = BASEURL

const jieyouApi = {

  /** 荣誉榜 */
  queryHonorAll() {
    return myaxios.get(BMDURL+'/ryall');
  },

  /** 查询8条数据 */
  queryid() {
    return myaxios.get(BMDURL+'/call');
  },

  /** 查询所有 */
  queryAll() {
    return myaxios.get(BMDURL+'/ball');
  },


  queryXinAll() {
    let url = BMDURL + "/daily";
    return myaxios.get(url);
  },
  /**
 *  通过uid查询咨询信
 * @param {Object} params 
 * @returns
 */
   queryByUidLike(params) {
    
    return myaxios.get(BMDURL + '/letter/query', params)
},
/**
 *  通过uid查询解忧信
 * @param {Object} params 
 * @returns
 */
 queryByUidaLike(params) {
    
  return myaxios.get(BMDURL + '/answer/query', params)
},
//根据id查询咨询信
 queryByIDLike(params) {
    
  return myaxios.get(BMDURL + '/ask/query', params)
},
//根据id查询解忧信
queryByIDaLike(params) {
    
  return myaxios.get(BMDURL + '/allay/query', params)
},
// 根据咨询查解忧
queryByaIDLike(params){
  return myaxios.get(BMDURL + '/sorrowlanswer', params)
},

  /**接收 id */
  queryAllid(params) {
    return myaxios.get(BMDURL + "/pess/id", params);
  },
  /**
 *  通过uid查询解忧信
 * @param {Object} params 
 * @returns
 */
   queryByUidaLike(params) {
    
    return myaxios.get(BMDURL + '/answer/query', params)
},
//新增解忧信
  addsorrowl(params){
    return myaxios.post(BMDURL + '/sorrowl/add', params)
  },
  //新增咨询信
  addgive(params){
    return myaxios.post(BMDURL + '/give/add', params)
  },
  /**
   *
   * @returns
   *
   *
   */
  /** 新增动态 */
  add(params) {
    return myaxios.post(BMDURL + "/daily/add", params);
  },

  /** 按cid查询评论 */
  querypinglun(params) {
    return myaxios.get(BMDURL + "/pinglun", params);
  },
  /**
   *
   * @returns
   *
   *
   */
  /********测试*** */

  /** 查询所有 未使用*/
  queryUid() {
    return myaxios.get(BMDURL + `/ball?id=${this.$route.query.id}`);
  },

  /**测试 接收 */
};

export default jieyouApi;