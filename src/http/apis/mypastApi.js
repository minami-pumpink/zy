/** 该文件仅存放演员模块相关的接口API */
import myaxios from "../MyAxios";
// 引入路径前缀
import BASEURL from '../BaseUrl' 
const {BMDURL} = BASEURL

const pastApi = {

  /** 查询过去的接口 */
  queryAll() {
    return myaxios.get(BMDURL+'/pass');
  },

  /**查询未来的接口 */
  queryAllTypes() {
    return myaxios.get(BMDURL+'/dream');
  },

  /** 写给过去传参接口*/ 
  queryAllById(params) {
    return myaxios.get(BMDURL+'/add/Addid',params)
  },

  /** 写给未来传参接口*/ 
  queryFuter(params) {
    return myaxios.get(BMDURL+'/news/newid',params)
  }
}

export default pastApi;