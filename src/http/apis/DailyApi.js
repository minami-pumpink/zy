import myaxios from "../MyAxios";
import BASEURL from '../BaseUrl'
const {BMDURL} = BASEURL

const dailyApi={

  // 根据id 查询详情
  queryByIDLike(params) {
     return myaxios.get(BMDURL + '/daily/ID', params)
    },


    // 查询所有日记
  queryAll() {
    let url = BMDURL + '/daily'
    return myaxios.get(url)
  },
    /**
   * 新增日记
   * @param {Object} params 
   */
  add(params) {
    return myaxios.post(BMDURL + '/daily/add', params)
  },
    /**
   *  通过标题查询日记
   * @param {Object} params 
   * @returns
   */
     queryByTitleLike(params) {
      
        return myaxios.get(BMDURL + '/daily/query', params)
    },

/**
   * 删除日记
   * @param {Object} params 
   */
  delete(params) {
    //console.log(222222,params);
    return myaxios.post(BMDURL + '/daily/del', params)
  },

}
export default dailyApi