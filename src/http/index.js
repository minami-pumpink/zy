import adminApi from "./apis/AdminApi";
import personApi from "./apis/PersonApi";

// 我的
import dynamicApi from "./apis/DynamicApi";
import dailyApi from './apis/DailyApi';
import fmApi from './apis/FMApi';
// 解忧
import jieyouApi from './apis/JieyouApi';
// 我的-过去和未来
import mypastApi from './apis/mypastApi'
// 心情
import moodApi from './apis/MoodApi';

const httpApi = {
  adminApi,
  personApi,
  // 我的
  dynamicApi,
  dailyApi,
  fmApi,
  // 解忧
  jieyouApi,
  // 我的-过去和未来
  mypastApi,
  // 心情
  moodApi,
  }

export default httpApi;
