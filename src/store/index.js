import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  // 此处用于存储全局共享的状态数据
  state: {
    // 读，当浏览器刷新时，初始化为storage中的用户
    user:JSON.parse(sessionStorage.getItem('user')),
    num:0
  },

  // 用于提供一些方法，方便的获取state中的临时计算结果
  // 类似于vue中的计算属性computed
  getters: {
  },

  // 用于提供一些方法，修改state中的状态数据
  // 管理员
  mutations: {
    // 通过以下代码调用
    // this.$store.commit（'updatePerson',{}）
    // 本来就有的 我传的
    updatePerson(state,newperson){
      state.user = newperson // 将用户传过来的登录成功的信息，存入全局
      // 存入sessionStorage
      // 还要读
      sessionStorage.setItem('user',JSON.stringify(newperson))
    },
    updatepingnum(state,pingnum){
      state.num = pingnum // 将用户传过来的登录成功的信息，存入全局
      // 存入sessionStorage
      // 还要读
    }
  },

  // 封装异步任务
  actions: {
  },

  // 封装模块
  modules: {
  }
})
