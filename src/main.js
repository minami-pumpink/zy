import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant'

/** 引入全局变量，方便组件调用模块API */
Vue.prototype.$http = httpApi
import httpApi from '@/http/index'

import './assets/css/reset.css'
import './assets/css/base.css'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';


import 'vant/lib/index.css'
import 'lib-flexible'

import moment from 'moment'
import 'moment/locale/zh-cn' //获取中国标准时间爱你，避免出现utc时间
Vue.prototype.moment = moment

Vue.use(ElementUI);
Vue.use(Vant)


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
